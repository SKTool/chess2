# chess2

## 체스게임 (명령창) ( web 변경 전 )
[chess_1](https://gitlab.com/SKTool/chess_1/tree/feature/fixMoveAndCapture)
특별한 뷰가 없고 명령창으로 게임을 진행하는 프로그램이다.
응용프로그램에서 웹으로 변경

## 응용프로그래밍(명령창) 에서 웹서버로 변경 
11월 01일 ~ 11월 30 일 (수업 일정때문에... 잠시 중지됨)

## 초기 화면
![chess](./blog/chess_init_page.PNG)

## 진행중인 화면
![chess](./blog/chess_2.PNG )

## 추가할 규칙
1. 양파상
3. 캐슬링
4. 체크메이트
5. 승리 조건 
6. ...

