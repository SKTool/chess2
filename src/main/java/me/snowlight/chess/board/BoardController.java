package me.snowlight.chess.board;

import java.util.Map;

import me.snowlight.chess.player.piece.Piece;
import me.snowlight.chess.util.Point;

public interface BoardController {
	public void settingPlayerPiece(Map<String, Piece> pieces);
	public void localPiece(Piece piece, Point toPoint);
	public Piece[][] getCopedBoard();
	public void viewBoard();
	public BoardModel getBoard();
}
