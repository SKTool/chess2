package me.snowlight.chess.board;

import me.snowlight.chess.player.piece.Piece;

public class BoardModel {
	Piece[][] board = new Piece[8][8];

	public Piece[][] getBoard() {
		return board;
	}
	
	public void setBoard(Piece[][] board) {
		this.board = board;
	}
	
}
