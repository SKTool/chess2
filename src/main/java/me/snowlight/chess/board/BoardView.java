package me.snowlight.chess.board;

public interface BoardView {
	public void viewBoard();
	public BoardModel getBoardModel();
}
