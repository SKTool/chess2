package me.snowlight.chess.board;

import java.util.Iterator;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import me.snowlight.chess.player.piece.Piece;
import me.snowlight.chess.util.Point;


public class BoardControllerImpl implements BoardController{

	Piece[][] board = null;
	
	BoardModel boardModel = new BoardModel();
	BoardView boardView = new BoardViewImpl(boardModel.getBoard());
	
	public BoardControllerImpl() {
		this.board = boardModel.getBoard();		

	}

	@Override
	public void settingPlayerPiece(Map<String, Piece> pieces) {
		
		Iterator<String> keys = pieces.keySet().iterator();
		
		while (keys.hasNext()) {
			String key = keys.next();
			board[pieces.get(key).getPointOfPiece().getRankIndex()][pieces.get(key).getPointOfPiece().getFileIndex()] = pieces.get(key);
		}
	}
	
	@Override
	public void localPiece(Piece piece, Point toPoint) {
		if ( piece.getStatus() != Piece.STOP) {
			board[piece.getPointOfPiece().getRankIndex()][piece.getPointOfPiece().getFileIndex()] = null;
			
			Point movedPoint = new Point(toPoint.getRankIndex(), toPoint.getFileIndex());
			piece.setPointOfPiece(movedPoint);
			
			captionPiece(piece, toPoint);
		}
	}

	private void captionPiece(Piece piece, Point toPoint) {
//		board[piece.getPointOfPiece().getRankIndex()][piece.getPointOfPiece().getFileIndex()] = null;
//		Point movedPoint = new Point(toPoint.getRankIndex(), toPoint.getFileIndex());
//		piece.setPointOfPiece(movedPoint);
		if (piece.getStatus() == piece.PASSANT) {
			board[toPoint.getRankIndex()][toPoint.getFileIndex()] = null;
			if (piece.isBlack())
				board[toPoint.getRankIndex()-1][toPoint.getFileIndex()] = piece;
			else 
				board[toPoint.getRankIndex()+1][toPoint.getFileIndex()] = piece;
		} else {
			board[toPoint.getRankIndex()][toPoint.getFileIndex()] = piece;
		}
	}

	@Override
	public Piece[][] getCopedBoard() {
		/*
		 * TODO
		 * * 복사해서 반환한다. ( 현재는 빠르게 테스트하기 위해서 그냥 전달 )
		 */
		return board;
	}

	@Override
	public void viewBoard() {
		boardView.viewBoard();
	}
	
	@Override
	public BoardModel getBoard() {
		return boardView.getBoardModel();
	}
}
