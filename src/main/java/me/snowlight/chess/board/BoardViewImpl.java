package me.snowlight.chess.board;

import me.snowlight.chess.player.piece.Piece;

public class BoardViewImpl implements BoardView {
	
	Piece[][] board = null;
	
	public BoardViewImpl(Piece[][] board) {
		this.board = board;
	}
	
	@Override
	public void viewBoard() {
		System.out.println();
		System.out.println("|~~~~~~~~~~~~~~~~~~~~~~~|");
		System.out.println();
		for (int i = 0 ; i < board.length  ; i++ ) {
			System.out.println("|-----------------------|");
			for (int j = 0 ; j < board[0].length ; j++ ) {
				System.out.print("|");
				if (board[i][j] == null) {
					System.out.print("  ");
				} else {
					System.out.print(board[i][j].getName());					
				}
			}
			System.out.print("|");
			System.out.println();
		}
	}
	
	public BoardModel getBoardModel() {
		BoardModel boardModel = new BoardModel();
		boardModel.setBoard(board);
		return boardModel;
	}
}
