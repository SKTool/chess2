package me.snowlight.chess.master;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import me.snowlight.chess.board.BoardController;
import me.snowlight.chess.board.BoardControllerImpl;
import me.snowlight.chess.board.BoardModel;
import me.snowlight.chess.player.PlayerViewImple;
import me.snowlight.chess.rule.Rule;


@Controller
@RequestMapping("/master")
public class MasterController {
	private BoardController board;

	@Autowired
	private Rule rule;

	private PlayerViewImple playerBlack;

	private PlayerViewImple playerWhite;
	
	@Autowired
	public void setPlayerBlack(PlayerViewImple playerBlack) {
		playerBlack.setColor("BLACK");
		playerBlack.setRule(rule);
		
		this.playerBlack = playerBlack;
	}
	
	@Autowired
	public void setPlayerWhite(PlayerViewImple playerWhite) {
		playerWhite.setColor("WHITE");
		playerWhite.setRule(rule);
		this.playerWhite = playerWhite;
	}

//	public MasterController() {
//		playerBlack.setColor("BLACK");
//		playerWhite.setColor("WHITE");
//		playerBlack.setRule(rule);
//		playerWhite.setRule(rule);
//	}
	
	@GetMapping("")
	public String game(Model model) {
		board = new BoardControllerImpl();
		playerBlack.setUpPiece();
		playerWhite.setUpPiece();
		board.settingPlayerPiece(playerBlack.getPieces());
		board.settingPlayerPiece(playerWhite.getPieces());
		rule.initializeBoardForRule(board.getCopedBoard());
		
		model.addAttribute("board", board.getBoard());
		
		return "board/game";
	}
	
	@PostMapping("/run/{color}/{name-piece}/{rank}/{file}")
	@ResponseBody
	public BoardModel startGame( @PathVariable("name-piece") String namePiece, @PathVariable String color, @PathVariable String rank, @PathVariable String file ) {
//		board = new BoardControllerImpl();
//		rule = new RuleImpl();
//		playerBlack = new PlayerImpl("BLACK", rule);
//		playerWhite = new PlayerImpl("WHITE", rule);
		System.out.println(namePiece + color +rank + file);


//		while (true) {
//			rule.initializeBoardForRule(board.getCopedBoard());
//			board.viewBoard();
//			playerBlack.inputPoint(board);
//			board.viewBoard();
//			playerWhite.inputPoint(board);
//		}

		
		if ( color.toUpperCase().equals("BLACK") ) {
//			board.viewBoard();
			playerBlack.setNamePiece(namePiece);
			playerBlack.setNextLine1(rank);
			playerBlack.setNextLine2(file);
			playerBlack.inputPoint(board);
		} else if (color.toUpperCase().equals("WHITE")) {
//			board.viewBoard();
			playerWhite.setNamePiece(namePiece);
			playerWhite.setNextLine1(rank);
			playerWhite.setNextLine2(file);
			playerWhite.inputPoint(board);	
		}
//		if (playerWhite.controlPiece(toPoint, namePiece))
		return board.getBoard();
	}
}
