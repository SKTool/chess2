package me.snowlight.chess.util;


public class Point {
	private int fileIndex;
	private int rankIndex;
	
	private static final int BOARD_ADJUSTMENT = 8;
	
	public Point(int rank, int file) {
		setRankIndex(rank);
		setFileIndex(file);
	}
	public Point(String rank, String file) {
		setRank(rank);
		setFile(file);
	}
	public void setRank(String rank) {
		this.rankIndex = BOARD_ADJUSTMENT - Integer.parseInt(rank) ;
	}
	
	public void setRankIndex(int rankIndex) {
		this.rankIndex = rankIndex;
	}
	
	public int getRankIndex() {
		return this.rankIndex;
	}
	
	public void setFile(String file) {
		//toUpperCase() 문자로 변경하는 메서드
		this.fileIndex = (int)(file.toUpperCase().charAt(0) - 'A');
	}

	public void setFileIndex(int fileIndex) {
		this.fileIndex = fileIndex;
	}
	
	public int getFileIndex() {
		return this.fileIndex;
	}
}
