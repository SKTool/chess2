package me.snowlight.chess.player;

import java.util.Scanner;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import me.snowlight.chess.board.BoardController;
import me.snowlight.chess.player.piece.Piece;
import me.snowlight.chess.util.Point;

@Service
@Scope(value="prototype")
public class PlayerViewImple extends PlayerImpl {
	private String nextLine1;
	private String nextLine2;
	private String namePiece;

	//String rank, String file
	@Override
	public void inputPoint(BoardController board) {
//		Scanner input = new Scanner(System.in);		
		Piece srcPiece = null;
		Point toPoint;
//		while (true) {
//			namePiece = name;

		toPoint = new Point(nextLine1, nextLine2);

		srcPiece = controlPiece(toPoint, namePiece);
		
		if (!srcPiece.getStatus().equals(Piece.STOP)) 
			board.localPiece(srcPiece, toPoint);
				
//		}
//		board.localPiece(srcPiece, toPoint);
	}

	public String getNextLine1() {
		return nextLine1;
	}

	public void setNextLine1(String nextLine1) {
		this.nextLine1 = nextLine1;
	}

	public String getNextLine2() {
		return nextLine2;
	}

	public void setNextLine2(String nextLine2) {
		this.nextLine2 = nextLine2;
	}

	public String getNamePiece() {
		return namePiece;
	}

	public void setNamePiece(String namePiece) {
		this.namePiece = namePiece;
	}

	
	
}
