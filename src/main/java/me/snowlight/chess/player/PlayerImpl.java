package me.snowlight.chess.player;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.springframework.stereotype.Service;

import me.snowlight.chess.board.BoardController;
import me.snowlight.chess.player.piece.Bishop;
import me.snowlight.chess.player.piece.King;
import me.snowlight.chess.player.piece.Knight;
import me.snowlight.chess.player.piece.Pawn;
import me.snowlight.chess.player.piece.Piece;
import me.snowlight.chess.player.piece.Queen;
import me.snowlight.chess.player.piece.Rook;
import me.snowlight.chess.rule.Rule;
import me.snowlight.chess.util.Point;


public class PlayerImpl implements Player {

	private Map<String, Piece> pieces = new HashMap<String, Piece>();

	private String playerColor;

	public static final String PLAYER_BLACK = "BLACK";
	public static final String PLAYER_WHITE = "WHITE";

	private Rule rule;
	
	public PlayerImpl() {}
	
	public PlayerImpl(String color, Rule rule) {
		this.playerColor = color;
		this.rule = rule;
	}
	
	@Override
	public void setColor(String color) {
		this.playerColor = color;
	}

	@Override
	public void setRule(Rule rule) {
		this.rule = rule;
	}
	
	@Override
	public void setUpPiece() {
		initializePawns();
		initializeRooks();
		initializeKnight();
		initializeBishop();
		initializeQueen();
		initializeKing();
	}

	public void initializeRooks() {
		String name = "R%s";
		Point newPoint = new Point(0,0);

		if (playerColor.equals(this.PLAYER_BLACK))
			newPoint = new Point(7,0);

		Point point = newPoint;
		for (int i = 1; i<3; i++) {
			if (i == 2) {
				newPoint = initializeRooksAndKnightAndBishop(point);
			}
			initializePieces(new Rook(String.format(name, i)), newPoint);
		}
	}
	
	public void initializeKnight() {
		String name = "N%s";
		Point newPoint = new Point(0,1);

		if (playerColor.equals(this.PLAYER_BLACK)) 
			newPoint = new Point(7,1);

		Point point = newPoint;
		for (int i = 1; i<3; i++) {
			if (i == 2) {
				newPoint = initializeRooksAndKnightAndBishop(point);
			}
			initializePieces(new Knight(String.format(name, i)), newPoint);
		}
	}
	
	public void initializeBishop() {
		String name = "B%s";
		Point newPoint = new Point(0,2);

		if (playerColor.equals(this.PLAYER_BLACK)) 
			newPoint = new Point(7,2);
		
		Point point = newPoint;
		for (int i = 1; i<3; i++) {
			if (i == 2) {
				newPoint = initializeRooksAndKnightAndBishop(point);
			}
			initializePieces(new Bishop(String.format(name, i)), newPoint);
		}
	}

	public Point initializeRooksAndKnightAndBishop(Point point) {
		Point newPoint;
		newPoint = new Point(0, 0);
		newPoint.setRankIndex(point.getRankIndex());
		newPoint.setFileIndex(point.getFileIndex());
		int reverseLocation = 7 - newPoint.getFileIndex();
		newPoint.setFileIndex(reverseLocation);
		return newPoint;
	}
	
	public void initializeQueen() {
		String name = "Q1";
		int rankIndex = 0;
		if (playerColor.equals(this.PLAYER_BLACK)) 
			rankIndex = 7;

		Point point = new Point(rankIndex,3);
		
		initializePieces(new Queen(name), point);
	}

	public void initializeKing() {
		String name = "K1";
		int rankIndex = 0;
		if (playerColor.equals(this.PLAYER_BLACK)) 
			rankIndex = 7;

		Point point = new Point(rankIndex,4);

		initializePieces(new King(name), point);
	}
	
	public void initializePawns() {
		int rankIndex = 1;
		if (playerColor.equals(this.PLAYER_BLACK)) 
			rankIndex = 6;

		for (int i = 0; i<8; i++) {
			Point newPoint = new Point(rankIndex, i);
			initializePieces(new Pawn(String.format("P%s", i+1)), newPoint);
		}
	}
	
	private void initializePieces(Piece initPieces, Point initPoint) {
		initPieces.setColor(playerColor);
		initPieces.setPointOfPiece(initPoint);
		pieces.put(initPieces.getName(), initPieces);
	}
	
	@Override
	public void inputPoint(BoardController board) {
		Scanner input = new Scanner(System.in);		
		Piece srcPiece = null;
		Point toPoint;
		String namePiece;
		while (true) {
			namePiece = input.nextLine();
			toPoint = new Point(input.nextLine(), input.nextLine());

			srcPiece = controlPiece(toPoint, namePiece);
			
			if (!srcPiece.getStatus().equals(Piece.STOP)) 
				break;
			
		}
		
		board.localPiece(srcPiece, toPoint);
	}

	public Piece controlPiece(Point toPoint, String namePiece) {
		Piece srcPiece = pieces.get(namePiece);	
		srcPiece.movePiece(rule, toPoint);
		return srcPiece;
	}

	@Override
	public Map<String, Piece> getPieces() {
		return this.pieces;
	}





}
