package me.snowlight.chess.player;

import java.util.Map;

import me.snowlight.chess.board.BoardController;
import me.snowlight.chess.player.piece.Piece;
import me.snowlight.chess.rule.Rule;

public interface Player {
	public void setUpPiece();
	public void inputPoint(BoardController board);
	public Map<String, Piece> getPieces();
	
	public void setColor(String color);
	public void setRule(Rule rule);
}	
