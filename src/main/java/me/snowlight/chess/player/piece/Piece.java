package me.snowlight.chess.player.piece;

import me.snowlight.chess.rule.Rule;
import me.snowlight.chess.util.Point;

public abstract class Piece {
	protected String name;
	protected String color;
	protected String status;
	protected int pieceCount;

	protected Point pointOfPiece;
	
	protected int[][] range;

	public final static String INIT = "INIT";
	public final static String STOP = "STOP";
	public final static String MOVE = "MOVE";
	public final static String CAPTURE = "CAPTURE";
	public final static String PASSANT  = "PASSANT";
	
	protected boolean multiplicationPattern;
	protected boolean plusPattern;
	
	public boolean isMultiplicationPattern() {
		return multiplicationPattern;
	}

	public boolean isPlusPattern() {
		return plusPattern;
	}
	
	public Piece() {
		status = INIT;
	}
	
	public abstract void movePiece(Rule rule, Point toPoint);
	
	public void moveNormalPiece(Rule rule, Point toPoint) {
		if (rule.checkBarrier(this, toPoint)) {
			this.status = STOP;
			return;
		}
		move(rule, toPoint);
	}

	public void move(Rule rule, Point toPoint) {
		if (rule.checkMovement(this, toPoint) ) {
			this.status = MOVE;
			return;
		}else {
			if (rule.checkCapture(this, toPoint)) {
				this.status = CAPTURE;
				return;
			}
		}
		this.status = STOP;
	}
	
	public void increasePieceCount() {
		if (this.status != STOP) {
			pieceCount++;
		}
	}
	
	public String getStatus() {
		return this.status;
	}
	
//	public void setStatus(String status) {
//		this.status = status;
//	}
	
	public String getName() {
		return name;
	}
	
	void setName(String name) {
		this.name = name;
	}
	
	public boolean isBlack() {
		return this.color.equals("BLACK");
	}
	
	public void setColor(String color) {
		this.color = color;
	}

	public Point getPointOfPiece() {
		return pointOfPiece;
	}
	
	public void setPointOfPiece(Point pointOfPiece) {
		this.pointOfPiece = pointOfPiece;
	}
	
	public int[][] getRange() {
		return range;
	}
	
	void setRange(int[][] range) {
		this.range = range;
	}
}
