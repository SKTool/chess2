package me.snowlight.chess.player.piece;


import me.snowlight.chess.rule.Rule;
import me.snowlight.chess.util.Point;


public class Rook extends Piece {
	public Rook(String name){
		this.name = name;
		this.range = new int[][]{		
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 1,1,1,1,1,1,1,0,1,1,1,1,1,1,1 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0 }
		};		
		this.multiplicationPattern = false;
		this.plusPattern = true;
	}
	
	@Override
	public void movePiece(Rule rule, Point toPoint) {	
		this.moveNormalPiece(rule, toPoint);
		this.increasePieceCount();
	}	
	
}
