package me.snowlight.chess.player.piece;

import me.snowlight.chess.rule.Rule;
import me.snowlight.chess.util.Point;

public class Pawn extends Piece {
	public Pawn(String name) {
		this.name = name;
		this.range = new int[][] {
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
			{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }
			
		};
		this.multiplicationPattern = true;
		this.plusPattern = false;
	}

	private final static int PAWN_BLACK_RANGE_RANK = 6 - 1;
	private final static int PAWN_WHITE_RANGE_RANK = 8 + 1;

	private final static int PAWN_BLACK_ATTACH_RANGE_RANK = 7 - 1;
	private final static int PAWN_WHITE_ATTACH_RANGE_RANK = 7 + 1;

	@Override
	public void movePiece(Rule rule, Point toPoint) {
		if (this.status == INIT)
			settingFirstLocationRange();		
		else 
			settingNotFirstLocationRange();

		Point tempToPoint = null;
		if (!this.isBlack()) {
			//tempToPoint = new Point(toPoint.getRankIndex()-1,toPoint.getFileIndex());
			tempToPoint = toPoint;
			this.move(rule, tempToPoint);

			// 이동 불가능하고 잡아 먹는거만 가능
			range[9][7] = 0;
			range[8][7] = 0;
			range[PAWN_WHITE_ATTACH_RANGE_RANK][7] = 1;
			range[PAWN_WHITE_ATTACH_RANGE_RANK][6] = 1;
			range[PAWN_WHITE_ATTACH_RANGE_RANK][8] = 1;
		} else {
			//tempToPoint = new Point(toPoint.getRankIndex()+1,toPoint.getFileIndex());
			tempToPoint = toPoint;
			this.move(rule, tempToPoint);

			// 이동 불가능하고 잡아 먹는거만 가능
			range[5][7] = 0;
			range[6][7] = 0;
			range[PAWN_BLACK_ATTACH_RANGE_RANK][7] = 1;
			range[PAWN_BLACK_ATTACH_RANGE_RANK][6] = 1;
			range[PAWN_BLACK_ATTACH_RANGE_RANK][8] = 1;
		}

		if (status == STOP) {
			//move(rule, tempToPoint);
			if (!rule.checkCapture(this, tempToPoint)) {
				status = STOP;
			} else {
				status = CAPTURE;
			}
//			if (status != STOP) {
//				status = PASSANT;
//			}
		}


		if (!this.isBlack()) {
			range[PAWN_WHITE_ATTACH_RANGE_RANK][6] = 0;
			range[PAWN_WHITE_ATTACH_RANGE_RANK][8] = 0;
		} else {
			range[PAWN_BLACK_ATTACH_RANGE_RANK][6] = 0;
			range[PAWN_BLACK_ATTACH_RANGE_RANK][8] = 0;
		}

		this.increasePieceCount();
	}

	private void settingNotFirstLocationRange() {
		range[PAWN_WHITE_RANGE_RANK][7] = 0;
		range[PAWN_WHITE_RANGE_RANK][7] = 0;
	}

	private void settingFirstLocationRange() {
		if (!this.isBlack()) {
			range[PAWN_WHITE_RANGE_RANK][7] = 1;
			range[PAWN_BLACK_RANGE_RANK][7] = 0;
		} else {
			range[PAWN_WHITE_RANGE_RANK][7] = 0;
			range[PAWN_BLACK_RANGE_RANK][7] = 1;
		}
	}

	@Override
	public void setColor(String color)	{
		this.color = color;
		if (!isBlack()) {
			range[6][7] = 0;
			range[8][7] = 1;
		} else {
			range[8][7] = 0;
			range[6][7] = 1;
		}
	} 
}
