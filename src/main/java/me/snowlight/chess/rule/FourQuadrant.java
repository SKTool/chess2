package me.snowlight.chess.rule;

import java.util.ArrayList;
import java.util.List;

import me.snowlight.chess.player.piece.Piece;
import me.snowlight.chess.util.Point;

public class FourQuadrant {
	int fromFile;
	int fromRank;
	int toFile;
	int toRank;
	
	
	public String fourQuadrant(Piece piece, Point toPoint) {
		
		
		if((piece.getPointOfPiece().getFileIndex() - toPoint.getFileIndex()) < 0
		&& (piece.getPointOfPiece().getRankIndex() - toPoint.getRankIndex()) > 0) {
			
			return "NorthEast";
			
		}else if((piece.getPointOfPiece().getFileIndex() - toPoint.getFileIndex()) > 0
			  && (piece.getPointOfPiece().getRankIndex() - toPoint.getRankIndex()) > 0) {
			
			return "NorthWest";
			
		}else if((piece.getPointOfPiece().getFileIndex() - toPoint.getFileIndex()) > 0
			  && (piece.getPointOfPiece().getRankIndex() - toPoint.getRankIndex()) < 0) {
			
			return "SouthWest";
				
		}else if((piece.getPointOfPiece().getFileIndex() - toPoint.getFileIndex()) < 0
			  && (piece.getPointOfPiece().getRankIndex() - toPoint.getRankIndex()) < 0) {
			
			return "SouthEast";
				
		}else {
			return "";
		}
		
	}
	
	public List<List<Integer>> CheckAllNorthEast(int[][] rangeBoard, Piece piece, Point toPoint) {
		
		List<List<Integer>> filledBlankList = new ArrayList<List<Integer>>();
		List<Integer> bundle;
		
		fromFile = piece.getPointOfPiece().getFileIndex();
		fromRank = piece.getPointOfPiece().getRankIndex();
		toFile = toPoint.getFileIndex();
		toRank = toPoint.getRankIndex();
		
		int spaceBetween = Math.abs(fromFile - toFile) - 1; 
		
		for(int i = 1; i <= spaceBetween; i++) {
			if(rangeBoard[fromRank - i][fromFile + i] == 0) {
				bundle = new ArrayList<Integer>();
				bundle.add(fromRank - i);
				bundle.add(fromFile + i);
				filledBlankList.add(bundle);
			}
		}
		
		return  filledBlankList;
	}
	
	public List<List<Integer>> checkAllNorthWest(int[][] rangeBoard, Piece piece, Point toPoint) {
		
		List<List<Integer>> filledBlankList = new ArrayList<List<Integer>>();
		List<Integer> bundle;
		
		fromFile = piece.getPointOfPiece().getFileIndex();
		fromRank = piece.getPointOfPiece().getRankIndex();
		toFile = toPoint.getFileIndex();
		toRank = toPoint.getRankIndex();
		
		int spaceBetween = Math.abs(fromFile - toFile) - 1; 
		
		for(int i = 1; i <= spaceBetween; i++) {
			if(rangeBoard[fromRank - i][fromFile - i] == 0) {
				bundle = new ArrayList<Integer>();
				bundle.add(fromRank - i);
				bundle.add(fromFile - i);
				filledBlankList.add(bundle);
			}
		}
		
		return  filledBlankList;
	}
	
	public List<List<Integer>> checkAllSouthWest(int[][] rangeBoard,Piece piece, Point toPoint) {
		
		List<List<Integer>> filledBlankList = new ArrayList<List<Integer>>();
		List<Integer> bundle;
		
		fromFile = piece.getPointOfPiece().getFileIndex();
		fromRank = piece.getPointOfPiece().getRankIndex();
		toFile = toPoint.getFileIndex();
		toRank = toPoint.getRankIndex();
		
		int spaceBetween = Math.abs(fromFile - toFile) - 1; 
		
		for(int i = 1; i <= spaceBetween; i++) {
			if(rangeBoard[fromRank + i][fromFile - i] == 0) {
				bundle = new ArrayList<Integer>();
				bundle.add(fromRank + i);
				bundle.add(fromFile - i);
				filledBlankList.add(bundle);
			}
		}
		
		return  filledBlankList;
	}
	
	public List<List<Integer>> checkAllSouthEast(int[][] rangeBoard,Piece piece, Point toPoint) {
		
		List<List<Integer>> filledBlankList = new ArrayList<List<Integer>>();
		List<Integer> bundle;
		
		fromFile = piece.getPointOfPiece().getFileIndex();
		fromRank = piece.getPointOfPiece().getRankIndex();
		toFile = toPoint.getFileIndex();
		toRank = toPoint.getRankIndex();
		
		int spaceBetween = Math.abs(fromFile - toFile) - 1; 
		
		for(int i = 1; i <= spaceBetween; i++) {
			if(rangeBoard[fromRank + i][fromFile + i] == 0) {
				bundle = new ArrayList<Integer>();
				bundle.add(fromRank + i);
				bundle.add(fromFile + i);
				filledBlankList.add(bundle);
			}
		}
		
		return  filledBlankList;
	}
}