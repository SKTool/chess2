package me.snowlight.chess.rule;

import me.snowlight.chess.player.piece.Piece;
import me.snowlight.chess.util.Point;

public interface Rule {
	public void initializeBoardForRule(Piece[][] copyedBoard);
	public boolean checkBarrier(Piece piece, Point toPoint);
	public boolean checkMovement(Piece piece, Point toPoint);
	public boolean checkCapture(Piece piece, Point toPoint);
	public boolean checkPromotion(Piece piece, Point toPoint);
	public boolean checkCastling(Piece king, Point toPoint);
}
