package me.snowlight.chess.rule;

import java.util.ArrayList;
import java.util.List;

import me.snowlight.chess.player.piece.Piece;
import me.snowlight.chess.util.Point;

public class FourDirection {
	RuleImpl rule = new RuleImpl();
	int fromFile;
	int fromRank;
	int toFile;
	int toRank;
	
	
	public String fourDirection(Piece piece, Point toPoint) {
		
		if(piece.getPointOfPiece().getFileIndex() == toPoint.getFileIndex()) {
			if(piece.getPointOfPiece().getRankIndex() > toPoint.getRankIndex()) {
				return "North";
			}
		}
		if(piece.getPointOfPiece().getFileIndex() == toPoint.getFileIndex()) {
			if(piece.getPointOfPiece().getRankIndex() < toPoint.getRankIndex()) {
				return "South";
			}
		}
		if(piece.getPointOfPiece().getRankIndex() == toPoint.getRankIndex()) {
			if(piece.getPointOfPiece().getFileIndex() > toPoint.getFileIndex()) {
				return "West";
			}
		}
		if(piece.getPointOfPiece().getRankIndex() == toPoint.getRankIndex()) {
			if(piece.getPointOfPiece().getFileIndex() < toPoint.getFileIndex()) {
				return "East";
			}
		}
		
		return "";
		
	}
	
	public List<List<Integer>> checkAllNorth(int[][] rangeBoard, Piece piece, Point toPoint) {
		
		List<List<Integer>> filledBlankList = new ArrayList<List<Integer>>();
		List<Integer> bundle;
		
		fromFile = piece.getPointOfPiece().getFileIndex();
		fromRank = piece.getPointOfPiece().getRankIndex();
		toFile = toPoint.getFileIndex();
		toRank = toPoint.getRankIndex();
		
		int spaceBetween = Math.abs(fromRank - toRank) - 1; 
		
		for(int i = 1; i <= spaceBetween; i++) {
			if(rangeBoard[fromRank - i][fromFile] == 0) {
				bundle = new ArrayList<Integer>();
				bundle.add(fromRank - i);
				bundle.add(fromFile);
				filledBlankList.add(bundle);
			}
		}
		
		return  filledBlankList;
	}
	
	public List<List<Integer>> checkAllSouth(int[][] rangeBoard, Piece piece, Point toPoint) {
		
		List<List<Integer>> filledBlankList = new ArrayList<List<Integer>>();
		List<Integer> bundle;
		
		fromFile = piece.getPointOfPiece().getFileIndex();
		fromRank = piece.getPointOfPiece().getRankIndex();
		toFile = toPoint.getFileIndex();
		toRank = toPoint.getRankIndex();
		
		int spaceBetween = Math.abs(fromRank - toRank) - 1; 
		
		for(int i = 1; i <= spaceBetween; i++) {
			if(rangeBoard[fromRank + i][fromFile] == 0) {
				bundle = new ArrayList<Integer>();
				bundle.add(fromRank + i);
				bundle.add(fromFile);
				filledBlankList.add(bundle);
			}
		}
		
		return  filledBlankList;
	}
	
	public List<List<Integer>> checkAllWest(int[][] rangeBoard, Piece piece, Point toPoint) {
		
		List<List<Integer>> filledBlankList = new ArrayList<List<Integer>>();
		List<Integer> bundle;
		
		fromFile = piece.getPointOfPiece().getFileIndex();
		fromRank = piece.getPointOfPiece().getRankIndex();
		toFile = toPoint.getFileIndex();
		toRank = toPoint.getRankIndex();
		
		int spaceBetween = Math.abs(fromFile - toFile) - 1; 
		
		for(int i = 1; i <= spaceBetween; i++) {
			if(rangeBoard[fromRank][fromFile - i] == 0) {
				bundle = new ArrayList<Integer>();
				bundle.add(fromRank);
				bundle.add(fromFile - i);
				filledBlankList.add(bundle);
			}
		}
		
		return  filledBlankList;
	}
	
	public List<List<Integer>> checkAllEast(int[][] rangeBoard, Piece piece, Point toPoint) {
		
		List<List<Integer>> filledBlankList = new ArrayList<List<Integer>>();
		List<Integer> bundle;
		
		fromFile = piece.getPointOfPiece().getFileIndex();
		fromRank = piece.getPointOfPiece().getRankIndex();
		toFile = toPoint.getFileIndex();
		toRank = toPoint.getRankIndex();
		
		int spaceBetween = Math.abs(fromFile - toFile)-1; 
		
		for(int i = 1; i <= spaceBetween; i++) {
			if(rangeBoard[fromRank][fromFile + i] == 0) {
				bundle = new ArrayList<Integer>();
				bundle.add(fromRank);
				bundle.add(fromFile + i);
				filledBlankList.add(bundle);
			}
		}
		
		return  filledBlankList;
	}	
}