package me.snowlight.chess.rule;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.ServerEndpoint;

import org.springframework.stereotype.Service;

import me.snowlight.chess.player.piece.Piece;
import me.snowlight.chess.util.Point;

@Service
public class RuleImpl implements Rule {
	
	private final static int OVERLAPPING_WITH_MOVEMENT_RANGE = 7;
	private int guardLineRank;
	private int guardLineFile;
	private Piece[][] board;	
	
	@Override
	public void initializeBoardForRule(Piece[][] copyedBoard) {
		this.board = copyedBoard;
		
	}
	
	
	@Override
	public boolean checkBarrier(Piece piece, Point toPoint) {					
		
		FourQuadrant fq = new FourQuadrant();
		FourDirection fd = new FourDirection();
		
		List<List<Integer>> filledBlankList = new ArrayList<List<Integer>>();
		
		int[][] rangeBoard = makeRangeBoard(piece);
		
		if(piece.isMultiplicationPattern()) {
			if(fq.fourQuadrant(piece, toPoint).equals("NorthEast")){
				
				filledBlankList = fq.CheckAllNorthEast(rangeBoard, piece, toPoint);
				
			}else if(fq.fourQuadrant(piece, toPoint).equals("NorthWest")){
				
				filledBlankList = fq.checkAllNorthWest(rangeBoard, piece, toPoint);
				
			}else if(fq.fourQuadrant(piece, toPoint).equals("SouthWest")){
				
				filledBlankList = fq.checkAllSouthWest(rangeBoard, piece, toPoint);
				
			}else if(fq.fourQuadrant(piece, toPoint).equals("SouthEast")){
				
				filledBlankList = fq.checkAllSouthEast(rangeBoard, piece, toPoint);
				
			}
		}else if(piece.isPlusPattern()) {
			if(fd.fourDirection(piece, toPoint).equals("North")) {
				
				filledBlankList = fd.checkAllNorth(rangeBoard, piece, toPoint);
				
			}else if(fd.fourDirection(piece, toPoint).equals("South")) {
				
				filledBlankList = fd.checkAllSouth(rangeBoard, piece, toPoint);
				
				
			}else if(fd.fourDirection(piece, toPoint).equals("West")) {
				
				filledBlankList = fd.checkAllWest(rangeBoard, piece, toPoint);
				
				
			}else if(fd.fourDirection(piece, toPoint).equals("East")) {
				
				filledBlankList = fd.checkAllEast(rangeBoard, piece, toPoint);
				
			}
		}
		if(filledBlankList.isEmpty() == true) {
			return false;
		}else {
			return true;			
		}
	}

	
	@Override
	public boolean checkMovement(Piece srcPiece, Point toPoint) {		
		
		if ( checkPieceRange(srcPiece, toPoint) && ( !isPieceOnBoard(toPoint) ) ) {
			return true;
		}
		return false;
	}
	
	
	public boolean checkPieceRange(Piece srcPiece, Point toPoint) {
		
		guardLineRank = toPoint.getRankIndex() + ( OVERLAPPING_WITH_MOVEMENT_RANGE - srcPiece.getPointOfPiece().getRankIndex() );
		guardLineFile = toPoint.getFileIndex() + ( OVERLAPPING_WITH_MOVEMENT_RANGE - srcPiece.getPointOfPiece().getFileIndex() );
		
		if ( srcPiece.getRange()[guardLineRank][guardLineFile] == 1) {
			return true;
		}
		
		return false;
	}
	
	
	public boolean isPieceOnBoard(Point toPoint) {
		
		if ( board[toPoint.getRankIndex()][toPoint.getFileIndex()] == null  ) {
			return false;
		}
		
		return true;
	}

	
	@Override
	public boolean checkCapture(Piece piece, Point toPoint) {		

		if (!checkPieceRange(piece, toPoint) ) {
			return false;
		}
		if (board[toPoint.getRankIndex()][toPoint.getFileIndex()] == null ) {
			return false;
		}
		if( board[toPoint.getRankIndex()][toPoint.getFileIndex()].isBlack() == piece.isBlack()) {
			return false;
		}
		
		return true;
		
	}

	
	@Override
	public boolean checkPromotion(Piece piece, Point toPoint) {					
		
		if(piece.isBlack() == true) {											
			if(toPoint.getRankIndex() == 0) {
				return true;
			}
		}else {
			if(toPoint.getRankIndex() == 7) {
				return true;
			}
		}
		return false;
	}
	
	
	public int[][] makeRangeBoard(Piece piece){
		
		Point point;
		int[][] arr = new int[8][8];
		
		for(int i = 0 ; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				
				point = new Point(i,j);
				
				if(checkPieceRange(piece, point) == true && isPieceOnBoard(point) == false) {
					arr[i][j] = 1;
				}
				
			}
		}

		return arr;
	}
	
	@Override
	public boolean checkCastling(Piece king, Point toPoint) {
		
		Piece rightBlackRook = this.board[7][7];
		Piece leftBlackRook = this.board[7][0];
		Piece rightWhiteRook = this.board[0][7];
		Piece leftWhiteRook = this.board[0][0];
		
		if(king.isBlack() == true) {
			if(toPoint.getFileIndex() > king.getPointOfPiece().getFileIndex()) {
				
				if(king.getStatus() == "INIT" && rightBlackRook.getStatus() == "INIT") {
					
					Point point1 = new Point(7, 6);
					Point point2 = new Point(7, 5);
					
					if(isPieceOnBoard(point1) == false && isPieceOnBoard(point2) == false) {
						return true;
					}
					
				}	
			}else if(toPoint.getFileIndex() < king.getPointOfPiece().getFileIndex()) {
				
				 if(king.getStatus() == "INIT" && leftBlackRook.getStatus() == "INIT") {
					
					Point point1 = new Point(7, 1);
					Point point2 = new Point(7, 2);
					Point point3 = new Point(7, 3);
					
					if(isPieceOnBoard(point1) == false && isPieceOnBoard(point2) == false && isPieceOnBoard(point3) == false) {
						return true;
					}
					
				}
			}
		}else {
			if(toPoint.getFileIndex() > king.getPointOfPiece().getFileIndex()) {
				
				if(king.getStatus() == "INIT" && rightWhiteRook.getStatus() == "INIT") {
					
					Point point1 = new Point(0, 6);
					Point point2 = new Point(0, 5);
					
					if(isPieceOnBoard(point1) == false && isPieceOnBoard(point2) == false) {
						return true;
					}
					
				}	
			}else if(toPoint.getFileIndex() < king.getPointOfPiece().getFileIndex()) {
				
				 if(king.getStatus() == "INIT" && leftWhiteRook.getStatus() == "INIT") {
					
					Point point1 = new Point(0, 1);
					Point point2 = new Point(0, 2);
					Point point3 = new Point(0, 3);
					
					if(isPieceOnBoard(point1) == false && isPieceOnBoard(point2) == false && isPieceOnBoard(point3) == false) {
						return true;
					}
					
				}
			}
		}
		return false;
	}

}
